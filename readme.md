# CIGeoE Translate to fit to adjacent polygon

Do a polygon translation to the nearest polygon, by making it coincide their nearest vertices

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “CIGeoETranslateToFitToAdjacentPolygon” to folder:

  ```console
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  ![ALT](./images/image01.png)

  3 - Select “CIGeoE Translate To Fit To Adjacent Polygon”

  ![ALT](./images/image02.png)

  4 - After confirm the operation the plugin will be available in toolbar

  ![ALT](./images/image03.png)

- Method 2:

  1 - Compress the folder “cigeoe_toggle_vertex_visibility” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip” (choose .zip generated in point 1) and then select "Install Plugin"



# Usage

1 - Clicking on the plugin icon opens a widget with instructions: "selecionar primeiro vértice" [select the first vertex] or "selecionar segundo vértice" [select the second vertex]. 

![ALT](./images/image04.png)

2 - Click on the vertex you want to move.

![ALT](./images/image05.png)

3 - Then, click on the second poligon vertex. This position is where you want to move the first polygon. After the click, the two vertices became coincidental.

![ALT](./images/image06.png)



# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
