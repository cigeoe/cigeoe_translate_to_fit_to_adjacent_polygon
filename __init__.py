# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CIGeoETranslateToFitToAdjacentPolygon
                                 A QGIS plugin
 Do a polygon translation to the nearest polygon, by making it coincide their nearest vertices
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                             -------------------
        begin                : 2019-02-06
        copyright            : (C) 2019 by Centro de Informação Geoespacial do Exército 
        email                : igeoe@igeoe.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load CIGeoETranslateToFitToAdjacentPolygon class from file CIGeoETranslateToFitToAdjacentPolygon.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .CIGeoE_Translate_To_Fit_To_Adjacent_Polygon import CIGeoETranslateToFitToAdjacentPolygon
    return CIGeoETranslateToFitToAdjacentPolygon(iface)
